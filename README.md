# Notes on `git lfs`

Homepage: https://git-lfs.github.com/ .

## Usage
* To init for a repo, run: `git lfs install`.

## Questions
* How well does it work on Windows?
* Does one have to run `git lfs install` after cloning?
* Is it possible to prune old versions on remote?
* Is the `git push` lag caused by `git lfs`?

## Notes
* Required admin privs to install.
* `git` 64bit has to be installed to support files larger than 2 GB.
* Files have to be "unlocked" to modifiable.






